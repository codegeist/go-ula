package ula

import (
	"crypto/sha1"
	"encoding/binary"
	"fmt"
	"github.com/denisbrodbeck/machineid"
	"math/rand"
	"net"
	"time"
)

var (
	netmask64 = []byte{0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0, 0, 0, 0, 0, 0, 0, 0}
)

func randomLocalMAC() ([]byte, error) {
	interfaces, err := net.Interfaces()
	if err != nil {
		return nil, fmt.Errorf("failure to retrieve local interfaces: %w", err)
	}

	var addrs []net.HardwareAddr
	for _, interf := range interfaces {
		if interf.HardwareAddr != nil && interf.HardwareAddr[0] != 0 {
			addrs = append(addrs, interf.HardwareAddr)
		}
	}

	if len(addrs) < 1 {
		return nil, fmt.Errorf("no valid MAC addresses found")
	}

	rand.Seed(time.Now().UnixNano())
	ridx := rand.Intn(len(addrs))
	return addrs[ridx], nil
}

func mac48ToEUI64(mac net.HardwareAddr) net.HardwareAddr {
	if len(mac) != 6 {
		return mac
	}

	var result net.HardwareAddr
	result = append(result, mac[0]|2, mac[1], mac[2], 0xff, 0xfe, mac[3], mac[4], mac[5])

	return result
}

func machineId() ([]byte, error) {
	id, err := randomLocalMAC()
	if err != nil {
		sid, err := machineid.ID()
		if err != nil {
			return nil, fmt.Errorf("unable to retrieve local MAC or machine ID: %w", err)
		}
		id = []byte(sid)
	} else {
		id = mac48ToEUI64(id)
	}
	return id, nil
}

// New creates a new RFC4193 Unique Local Address prefix
func New() (*net.IPNet, error) {
	sha := sha1.New()

	b := make([]byte, 8)
	// technically not correct, since RFC4193 calls for NTP time
	binary.BigEndian.PutUint64(b, uint64(time.Now().UnixNano()))
	sha.Write(b)

	mid, err := machineId()
	if err != nil {
		return nil, err
	}
	sha.Write(mid)

	var ip net.IP
	ip = append(ip, 0xfd)                 // RFC4193 prefix with L-bit set
	ip = append(ip, sha.Sum(nil)[15:]...) // pseudo-random ID, aka sufficiently random ID
	ip = append(ip, 0x00, 1)              // subnet ID set to 1 for the first subnet
	ip = append(ip, make([]byte, 8)...)   // 64 bits of zeroes, to be used for client addresses

	return &net.IPNet{
		IP:   ip,
		Mask: netmask64,
	}, nil
}

// NextSubnet takes an existing ULA, increments the subnet ID and returns a new subnet in the same ULA block
func NextSubnet(ip *net.IPNet) (*net.IPNet, error) {
	if ip.IP[0] != 0xfd || len(ip.IP) != net.IPv6len {
		return nil, fmt.Errorf("invalid address")
	}

	i := binary.BigEndian.Uint16(ip.IP[6:8])
	i++
	binary.BigEndian.PutUint16(ip.IP[6:8], i)

	return ip, nil
}
