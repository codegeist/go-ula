# go-ula

## Description

Generate /64 subnets for Unique Local IPv6 Unicast Addresses (ULAs) (RFC4193). The library allows you to create subnet prefixes in the fd::/8 space, meaning the private subnet IDs are (probably) globally unique.

## Usage

```go
package main

import "gitlab.com/codegeist/go-ula"
import "fmt"
import "log"

func main() {
  ipnet, err := ula.New()
  if err != nil {
    log.Fatal(err)
  }
  fmt.Println(ipnet)

  ipnet, err = ula.NextSubnet(ipnet)
  if err != nil {
    log.Fatal(err)
  }
  fmt.Println(ipnet)
}
```

Example output:

    fd80:35d8:e7f2:1::/64
    fd80:35d8:e7f2:2::/64

## Author

Karsten Engelke

## License

Licensed under BSD 3-Clause "New" or "Revised" license.